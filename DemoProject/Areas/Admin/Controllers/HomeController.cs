﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoProject.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        Entities db = new Entities();
        //
        // GET: /Admin/Home/
        public ActionResult Index()
        {
            var news = db.News.Where(d => d.IsHidden = false).ToList();
            return View(news);
        }
	}
}